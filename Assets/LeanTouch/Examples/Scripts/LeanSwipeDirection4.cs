using UnityEngine;
using UnityEngine.UI;

namespace Lean.Touch
{
    // This script will tell you which direction you swiped in
    public class LeanSwipeDirection4 : MonoBehaviour
    {
        private bool isSwipedLeft = false;
        private bool isSwipedRight = false;
        private int speed = 100;
        protected virtual void OnEnable()
        {
            // Hook into the events we need
            LeanTouch.OnFingerSwipe += OnFingerSwipe;
        }

        protected virtual void OnDisable()
        {
            // Unhook the events
            LeanTouch.OnFingerSwipe -= OnFingerSwipe;
        }
        void Update()
        {
            if (isSwipedLeft && speed > 0)
            {
                GameObject.FindGameObjectWithTag("Sun_detail").transform.Rotate(0, 0, speed * Time.deltaTime);                
                GameObject.FindGameObjectWithTag("Jupiter_detail").transform.Rotate(0, 0, speed * Time.deltaTime);
                GameObject.FindGameObjectWithTag("Mars_detail").transform.Rotate(0, 0, speed * Time.deltaTime);
                GameObject.FindGameObjectWithTag("Mercury_detail").transform.Rotate(0, 0, speed * Time.deltaTime);
                GameObject.FindGameObjectWithTag("Neptune_detail").transform.Rotate(0, 0, speed * Time.deltaTime);
                GameObject.FindGameObjectWithTag("Uranus_detail").transform.Rotate(0, 0, speed * Time.deltaTime);
                GameObject.FindGameObjectWithTag("Pluto_detail").transform.Rotate(0, 0, speed * Time.deltaTime);
                GameObject.FindGameObjectWithTag("Saturn_detail").transform.Rotate(0, 0, speed * Time.deltaTime);
                GameObject.FindGameObjectWithTag("Saturn_Round_detail").transform.Rotate(0, 0, speed * Time.deltaTime);
                GameObject.FindGameObjectWithTag("Venus_detail").transform.Rotate(0, 0, speed * Time.deltaTime);
                GameObject.FindGameObjectWithTag("Earth_detail").transform.Rotate(0, 0, speed * Time.deltaTime);
                speed--;
            }
            else if(isSwipedRight && speed > 0)
            {
                GameObject.FindGameObjectWithTag("Sun_detail").transform.Rotate(0, 0, -speed * Time.deltaTime);
                GameObject.FindGameObjectWithTag("Jupiter_detail").transform.Rotate(0, 0, -speed * Time.deltaTime);
                GameObject.FindGameObjectWithTag("Mars_detail").transform.Rotate(0, 0, -speed * Time.deltaTime);
                GameObject.FindGameObjectWithTag("Mercury_detail").transform.Rotate(0, 0, -speed * Time.deltaTime);
                GameObject.FindGameObjectWithTag("Neptune_detail").transform.Rotate(0, 0, -speed * Time.deltaTime);
                GameObject.FindGameObjectWithTag("Uranus_detail").transform.Rotate(0, 0, -speed * Time.deltaTime);
                GameObject.FindGameObjectWithTag("Pluto_detail").transform.Rotate(0, 0, -speed * Time.deltaTime);
                GameObject.FindGameObjectWithTag("Saturn_detail").transform.Rotate(0, 0, -speed * Time.deltaTime);
                GameObject.FindGameObjectWithTag("Saturn_Round_detail").transform.Rotate(0, 0, -speed * Time.deltaTime);
                GameObject.FindGameObjectWithTag("Venus_detail").transform.Rotate(0, 0, -speed * Time.deltaTime);
                GameObject.FindGameObjectWithTag("Earth_detail").transform.Rotate(0, 0, -speed * Time.deltaTime);
                speed--;
            }
        }
        public void OnFingerSwipe(LeanFinger finger)
        {
            // Make sure the info text exists           

            // Store the swipe delta in a temp variable
            var swipe = finger.SwipeScreenDelta;
          
            if (swipe.x < -Mathf.Abs(swipe.y))
            {
                isSwipedLeft = true;
                isSwipedRight = false;
                speed = 100;            
            }

            if (swipe.x > Mathf.Abs(swipe.y))
            {
                isSwipedRight = true;
                isSwipedLeft = false;
                speed = 100;
            }            
        }
    }
}