﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using UnityEngine.UI;

public class Detail_View : MonoBehaviour {

    GameObject solarsystem,sundetails,canvasdetails, jupiterdetails, marsdetails,mercurydetails, neptunedetails, uranusdetails, plutodetails;
    GameObject saturndetails,saturnrounddetails,venusdetails, earthdetails;
    public int popupshow;
	GameObject Title,Distance,Hours,Days,Diameter,Temperature,Moons;
	GameObject Distanceinfo,Hoursinfo,Daysinfo,Diameterinfo,Temperatureinfo,Moonsinfo;
	private Text txt_title, txt_Distancefromsun, txt_Hoursperday, txt_Daysperyear, txt_Diameter, txt_Temperature, txt_Numberofmoons;
	private Text txt_Distanceinfo,txt_Hoursinfo,txt_Daysinfo,txt_Diameterinfo,txt_Temperatureinfo,txt_Moonsinfo;
    public AudioClip sfxclip;
    
    // Use this for initialization
    void Start()
    {
        solarsystem = GameObject.FindGameObjectWithTag("SS");

        canvasdetails = GameObject.FindGameObjectWithTag("Detail_Canvas");
        canvasdetails.GetComponent<Canvas>().enabled = false;

        sundetails = GameObject.FindGameObjectWithTag("Sun_detail");
        sundetails.GetComponent<MeshRenderer>().enabled = false;

        jupiterdetails = GameObject.FindGameObjectWithTag("Jupiter_detail");
        jupiterdetails.GetComponent<MeshRenderer>().enabled = false;

        marsdetails = GameObject.FindGameObjectWithTag("Mars_detail");
        marsdetails.GetComponent<MeshRenderer>().enabled = false;

        mercurydetails = GameObject.FindGameObjectWithTag("Mercury_detail");
        mercurydetails.GetComponent<MeshRenderer>().enabled = false;

        neptunedetails = GameObject.FindGameObjectWithTag("Neptune_detail");
        neptunedetails.GetComponent<MeshRenderer>().enabled = false;

        uranusdetails = GameObject.FindGameObjectWithTag("Uranus_detail");
        uranusdetails.GetComponent<MeshRenderer>().enabled = false;

        plutodetails = GameObject.FindGameObjectWithTag("Pluto_detail");
        plutodetails.GetComponent<MeshRenderer>().enabled = false;

        saturndetails = GameObject.FindGameObjectWithTag("Saturn_detail");
        saturndetails.GetComponent<MeshRenderer>().enabled = false;

        saturnrounddetails = GameObject.FindGameObjectWithTag("Saturn_Round_detail");
        saturnrounddetails.GetComponent<MeshRenderer>().enabled = false;

        venusdetails = GameObject.FindGameObjectWithTag("Venus_detail");
        venusdetails.GetComponent<MeshRenderer>().enabled = false;

        earthdetails = GameObject.FindGameObjectWithTag("Earth_detail");
        earthdetails.GetComponent<MeshRenderer>().enabled = false;

		txt_title=GameObject.FindGameObjectWithTag("Title").GetComponent<Text>();
		Title = GameObject.FindGameObjectWithTag ("Title");
		Title.GetComponent<Text>().enabled = false;

		txt_Distancefromsun=GameObject.FindGameObjectWithTag("Distancefromsun").GetComponent<Text>();
		Distance = GameObject.FindGameObjectWithTag ("Distancefromsun");
		Distance.GetComponent<Text>().enabled = false;
		txt_Distanceinfo=GameObject.FindGameObjectWithTag("Distanceinfo").GetComponent<Text>();
		Distanceinfo = GameObject.FindGameObjectWithTag ("Distanceinfo");
		Distanceinfo.GetComponent<Text>().enabled = false;

		txt_Hoursperday=GameObject.FindGameObjectWithTag("Hoursperday").GetComponent<Text>();
		Hours = GameObject.FindGameObjectWithTag ("Hoursperday");
		Hours.GetComponent<Text>().enabled = false;
		txt_Hoursinfo=GameObject.FindGameObjectWithTag("Hoursinfo").GetComponent<Text>();
		Hoursinfo = GameObject.FindGameObjectWithTag ("Hoursinfo");
		Hoursinfo.GetComponent<Text>().enabled = false;

		txt_Daysperyear=GameObject.FindGameObjectWithTag("Daysperyear").GetComponent<Text>();
		Days = GameObject.FindGameObjectWithTag ("Daysperyear");
		Days.GetComponent<Text>().enabled = false;
		txt_Daysinfo=GameObject.FindGameObjectWithTag("Daysinfo").GetComponent<Text>();
		Daysinfo = GameObject.FindGameObjectWithTag ("Daysinfo");
		Daysinfo.GetComponent<Text>().enabled = false;

		txt_Diameter=GameObject.FindGameObjectWithTag("Diameter").GetComponent<Text>();
		Diameter = GameObject.FindGameObjectWithTag ("Diameter");
		Diameter.GetComponent<Text>().enabled = false;
		txt_Diameterinfo=GameObject.FindGameObjectWithTag("Diameterinfo").GetComponent<Text>();
		Diameterinfo = GameObject.FindGameObjectWithTag ("Diameterinfo");
		Diameterinfo.GetComponent<Text>().enabled = false;

		txt_Numberofmoons=GameObject.FindGameObjectWithTag("Numberofmoons").GetComponent<Text>();
		Moons = GameObject.FindGameObjectWithTag ("Numberofmoons");
		Moons.GetComponent<Text>().enabled = false;
		txt_Moonsinfo=GameObject.FindGameObjectWithTag("Moonsinfo").GetComponent<Text>();
		Moonsinfo = GameObject.FindGameObjectWithTag ("Moonsinfo");
		Moonsinfo.GetComponent<Text>().enabled = false;

		txt_Temperature=GameObject.FindGameObjectWithTag("Temperature").GetComponent<Text>();
		Temperature = GameObject.FindGameObjectWithTag ("Temperature");
		Temperature.GetComponent<Text>().enabled = false;
		txt_Temperatureinfo=GameObject.FindGameObjectWithTag("Temperatureinfo").GetComponent<Text>();
		Temperatureinfo = GameObject.FindGameObjectWithTag ("Temperatureinfo");
		Temperatureinfo.GetComponent<Text>().enabled = false;



    }

    void OnMouseDown()
    {
        string tagname = this.tag;

        Debug.Log("The " + tagname + " was clicked");
        if (!IsPointerOverUIObject())            
        {

            Title.GetComponent<Text>().enabled = false;
            Distance.GetComponent<Text>().enabled = false;
            Hours.GetComponent<Text>().enabled = false;
            Days.GetComponent<Text>().enabled = false;
            Diameter.GetComponent<Text>().enabled = false;
            Moons.GetComponent<Text>().enabled = false;
            Temperature.GetComponent<Text>().enabled = false;

            Distanceinfo.GetComponent<Text>().enabled = false;
            Hoursinfo.GetComponent<Text>().enabled = false;
            Daysinfo.GetComponent<Text>().enabled = false;
            Diameterinfo.GetComponent<Text>().enabled = false;
            Moonsinfo.GetComponent<Text>().enabled = false;
            Temperatureinfo.GetComponent<Text>().enabled = false;

            solarsystem.GetComponent<Animator>().enabled = false; // pause the solar syatem
                                                                                             //GameObject.FindGameObjectWithTag("btn").GetComponent<Canvas>().enabled = true;
            canvasdetails.GetComponent<Canvas>().enabled = true;
            if (tagname == "Solar_sun")
			{
				StartCoroutine (Sun(0.2f));
                sundetails.GetComponent<MeshRenderer>().enabled = true;
            }
            else if (tagname == "solar_Jupiter")
            {
				StartCoroutine (Jupiter(0.2f));
                jupiterdetails.GetComponent<MeshRenderer>().enabled = true;
            }
            else if (tagname == "Solar_Mars")
            {
				StartCoroutine (Mars(0.2f));
               marsdetails.GetComponent<MeshRenderer>().enabled = true;
            }
            else if (tagname == "Solar_Mercury")
            {
				StartCoroutine (Mercury(0.2f));
               mercurydetails.GetComponent<MeshRenderer>().enabled = true;
            }
            else if (tagname == "Solar_Neptune")
            {
				StartCoroutine (Neptune(0.2f));
                neptunedetails.GetComponent<MeshRenderer>().enabled = true;
            }
            else if (tagname == "Solar_Uranus")
			{
				StartCoroutine (Uranus(0.2f));
                uranusdetails.GetComponent<MeshRenderer>().enabled = true;
            }
            else if (tagname == "Solar_pluto")
            {
				StartCoroutine (Pluto(0.2f));
                plutodetails.GetComponent<MeshRenderer>().enabled = true;
            }
            else if (tagname == "Solar_Saturn")
            {
				StartCoroutine (Saturan(0.2f));
                saturndetails.GetComponent<MeshRenderer>().enabled = true;
                saturnrounddetails.GetComponent<MeshRenderer>().enabled = true;
            }
            else if (tagname == "Solar_Venus")
            {
				StartCoroutine (Venus(0.2f));
                venusdetails.GetComponent<MeshRenderer>().enabled = true;
            }
            else if (tagname == "Solar_Earth")
            {
				StartCoroutine (Earth(0.2f));
               earthdetails.GetComponent<MeshRenderer>().enabled = true;
            }
        }
        else
        {
            Debug.Log("Close Selected One");
        }
    }
    private bool IsPointerOverUIObject()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }
    public void Button_click()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
        //AudioSource.PlayClipAtPoint(sfxclip, Vector3.zero, 1.0f);
        solarsystem.GetComponent<Animator>().enabled = true;
        canvasdetails.GetComponent<Canvas>().enabled = false;
        Title.GetComponent<Text>().enabled = false;
        Distance.GetComponent<Text>().enabled = false;
        Hours.GetComponent<Text>().enabled = false;
        Days.GetComponent<Text>().enabled = false;
		Diameter.GetComponent<Text>().enabled = false;
		Moons.GetComponent<Text>().enabled = false;
        Temperature.GetComponent<Text>().enabled = false;

		Distanceinfo.GetComponent<Text>().enabled = false;
		Hoursinfo.GetComponent<Text>().enabled = false;
		Daysinfo.GetComponent<Text>().enabled = false;
		Diameterinfo.GetComponent<Text>().enabled = false;
		Moonsinfo.GetComponent<Text>().enabled = false;
		Temperatureinfo.GetComponent<Text>().enabled = false;

        sundetails.GetComponent<MeshRenderer>().enabled = false;
        jupiterdetails.GetComponent<MeshRenderer>().enabled = false;
        marsdetails.GetComponent<MeshRenderer>().enabled = false;
        mercurydetails.GetComponent<MeshRenderer>().enabled = false;
        neptunedetails.GetComponent<MeshRenderer>().enabled = false;
        uranusdetails.GetComponent<MeshRenderer>().enabled = false;
        plutodetails.GetComponent<MeshRenderer>().enabled = false;
        saturndetails.GetComponent<MeshRenderer>().enabled = false;
        saturnrounddetails.GetComponent<MeshRenderer>().enabled = false;
        venusdetails.GetComponent<MeshRenderer>().enabled = false;
        earthdetails.GetComponent<MeshRenderer>().enabled = false;
        Debug.Log(popupshow);
    }

	IEnumerator Sun(float waitTime){

		Title.GetComponent<Text> ().enabled = true;
		txt_title.text = "SUN";
		yield return new WaitForSeconds (waitTime);

        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
       // AudioSource.PlayClipAtPoint(sfxclip, Vector3.zero, 1.0f);
        Diameter.GetComponent<Text> ().enabled = true;
		Diameterinfo.GetComponent<Text> ().enabled = true;
		txt_Diameter.text = "DIAMETER :";
		txt_Diameterinfo.text = "13,19,400 KILOMETERS";
		yield return new WaitForSeconds (waitTime);

        audio.Play();
        Temperature.GetComponent<Text> ().enabled = true;
		Temperatureinfo.GetComponent<Text> ().enabled = true;
		txt_Temperature.text = "TEMPERATURE :";
		txt_Temperatureinfo.text = "5503 DEGREE CELCIUS";
		yield return new WaitForSeconds (waitTime);

	}

    IEnumerator Mercury(float waitTime){
        Title.GetComponent<Text> ().enabled = true;
		txt_title.text = "MERCURY";
		yield return new WaitForSeconds (waitTime);

        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
        Distance.GetComponent<Text> ().enabled = true;
		Distanceinfo.GetComponent<Text> ().enabled = true;
		txt_Distancefromsun.text = "DISTANCE FROM SUN :";
		txt_Distanceinfo.text = " 58 MILLION KILOMETERS";
		yield return new WaitForSeconds (waitTime);

        audio.Play();
        Hours.GetComponent<Text> ().enabled = true;
		Hoursinfo.GetComponent<Text> ().enabled = true;
		txt_Hoursperday.text = "HOURS PER DAY :";
		txt_Hoursinfo.text = "59 EARTH DAYS";
		yield return new WaitForSeconds (waitTime);

        audio.Play();
        Days.GetComponent<Text> ().enabled = true;
		Daysinfo.GetComponent<Text> ().enabled = true;
		txt_Daysperyear.text = "DAYS PER YEAR :";
		txt_Daysinfo.text = "88 EARTH DAYS";
		yield return new WaitForSeconds (waitTime);

        audio.Play();
        Diameter.GetComponent<Text> ().enabled = true;
		Diameterinfo.GetComponent<Text> ().enabled = true;
		txt_Diameter.text = "DIAMETER :";
		txt_Diameterinfo.text = "4880 KILOMETERS";
		yield return new WaitForSeconds (waitTime);

        audio.Play();
        Moons.GetComponent<Text> ().enabled = true;
		Moonsinfo.GetComponent<Text> ().enabled = true;
		txt_Numberofmoons.text = "NUMBER OF MOONS :";
		txt_Moonsinfo.text = "NO MOON";
		yield return new WaitForSeconds (waitTime);

        audio.Play();
        Temperature.GetComponent<Text> ().enabled = true;
		Temperatureinfo.GetComponent<Text> ().enabled = true;
		txt_Temperature.text = "TEMPERATURE :";
		txt_Temperatureinfo.text = "400 DEGREE CELCIUS";
		yield return new WaitForSeconds (waitTime);

}

	IEnumerator Venus(float waitTime){
		Title.GetComponent<Text> ().enabled = true;
		txt_title.text = "VENUS";
		yield return new WaitForSeconds (waitTime);

        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
        Distance.GetComponent<Text> ().enabled = true;
		Distanceinfo.GetComponent<Text> ().enabled = true;
		txt_Distancefromsun.text = "DISTANCE FROM SUN :";
		txt_Distanceinfo.text = "108 MILLION KILOMETERS";
		yield return new WaitForSeconds (waitTime);

        audio.Play();
        Hours.GetComponent<Text> ().enabled = true;
		Hoursinfo.GetComponent<Text> ().enabled = true;
		txt_Hoursperday.text = "HOURS PER DAY :";
		txt_Hoursinfo.text = "243 EARTH DAYS";
		yield return new WaitForSeconds (waitTime);

        audio.Play();
        Days.GetComponent<Text> ().enabled = true;
		Daysinfo.GetComponent<Text> ().enabled = true;
		txt_Daysperyear.text = "DAYS PER YEAR :";
		txt_Daysinfo.text = "225 EARTH DAYS";
		yield return new WaitForSeconds (waitTime);

        audio.Play();
        Diameter.GetComponent<Text> ().enabled = true;
		Diameterinfo.GetComponent<Text> ().enabled = true;
		txt_Diameter.text = "DIAMETER :";
		txt_Diameterinfo.text = "12,000 KILOMETERS";
		yield return new WaitForSeconds (waitTime);

        audio.Play();
        Moons.GetComponent<Text> ().enabled = true;
		Moonsinfo.GetComponent<Text> ().enabled = true;
		txt_Numberofmoons.text = "NUMBER OF MOONS :";
		txt_Moonsinfo.text = "NO MOON";
		yield return new WaitForSeconds (waitTime);

        audio.Play();
        Temperature.GetComponent<Text> ().enabled = true;
		Temperatureinfo.GetComponent<Text> ().enabled = true;
		txt_Temperature.text = "TEMPERATURE :";
		txt_Temperatureinfo.text = "460 DEGREE CELCIUS";
		yield return new WaitForSeconds (waitTime);
	}

	IEnumerator Earth(float waitTime){
		
		Title.GetComponent<Text> ().enabled = true;
		txt_title.text = "EARTH";
		yield return new WaitForSeconds (waitTime);
                
        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();

        Distance.GetComponent<Text> ().enabled = true;
		Distanceinfo.GetComponent<Text> ().enabled = true;
		txt_Distancefromsun.text = "DISTANCE FROM SUN :";
		txt_Distanceinfo.text = "150 MILLION KILOMETERS";
		yield return new WaitForSeconds (waitTime);
        
        audio.Play();
        Hours.GetComponent<Text> ().enabled = true;
		Hoursinfo.GetComponent<Text> ().enabled = true;
		txt_Hoursperday.text = "HOURS PER DAY :";
		txt_Hoursinfo.text = "24 EARTH HOURS";
		yield return new WaitForSeconds (waitTime);
        
        audio.Play();
        Days.GetComponent<Text> ().enabled = true;
		Daysinfo.GetComponent<Text> ().enabled = true;
		txt_Daysperyear.text = "DAYS PER YEAR :";
		txt_Daysinfo.text = "365 EARTH DAYS";
		yield return new WaitForSeconds (waitTime);
                
        audio.Play();
        Diameter.GetComponent<Text> ().enabled = true;
		Diameterinfo.GetComponent<Text> ().enabled = true;
		txt_Diameter.text = "DIAMETER :";
		txt_Diameterinfo.text = "12,755 KILOMETERS";
		yield return new WaitForSeconds (waitTime);
                
        audio.Play();
        Moons.GetComponent<Text> ().enabled = true;
		Moonsinfo.GetComponent<Text> ().enabled = true;
		txt_Numberofmoons.text = "NUMBER OF MOONS :";
		txt_Moonsinfo.text = "1 MOON";
		yield return new WaitForSeconds (waitTime);
               
        audio.Play();
        Temperature.GetComponent<Text> ().enabled = true;
		Temperatureinfo.GetComponent<Text> ().enabled = true;
		txt_Temperature.text = "TEMPERATURE :";
		txt_Temperatureinfo.text = "22 DEGREE CELCIUS";
		yield return new WaitForSeconds (waitTime);
									
	}

	IEnumerator Mars(float waitTime){
		
		Title.GetComponent<Text> ().enabled = true;
		txt_title.text = "MARS";
		yield return new WaitForSeconds (waitTime);

        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
        Distance.GetComponent<Text> ().enabled = true;
		Distanceinfo.GetComponent<Text> ().enabled = true;
		txt_Distancefromsun.text = "DISTANCE FROM SUN :";
		txt_Distanceinfo.text = "223 MILLION KILOMETERS";
		yield return new WaitForSeconds (waitTime);

        audio.Play();
        Hours.GetComponent<Text> ().enabled = true;
		Hoursinfo.GetComponent<Text> ().enabled = true;
		txt_Hoursperday.text = "HOURS PER DAY :";
		txt_Hoursinfo.text = "24.5 EARTH HOURS";
		yield return new WaitForSeconds (waitTime);

        audio.Play();
        Days.GetComponent<Text> ().enabled = true;
		Daysinfo.GetComponent<Text> ().enabled = true;
		txt_Daysperyear.text = "DAYS PER YEAR :";
		txt_Daysinfo.text = "687 EARTH DAYS";
		yield return new WaitForSeconds (waitTime);

        audio.Play();
        Diameter.GetComponent<Text> ().enabled = true;
		Diameterinfo.GetComponent<Text> ().enabled = true;
		txt_Diameter.text = "DIAMETER :";
		txt_Diameterinfo.text = "6,750 KILOMETERS";
		yield return new WaitForSeconds (waitTime);

        audio.Play();
        Moons.GetComponent<Text> ().enabled = true;
		Moonsinfo.GetComponent<Text> ().enabled = true;
		txt_Numberofmoons.text = "NUMBER OF MOONS :";
		txt_Moonsinfo.text = "2 MOONS";
		yield return new WaitForSeconds (waitTime);

        audio.Play();
        Temperature.GetComponent<Text> ().enabled = true;
		Temperatureinfo.GetComponent<Text> ().enabled = true;
		txt_Temperature.text = "TEMPERATURE :";
		txt_Temperatureinfo.text = "-23 DEGREE CELCIUS";
		yield return new WaitForSeconds (waitTime);

	}

	IEnumerator Jupiter(float waitTime){
		
		Title.GetComponent<Text> ().enabled = true;
		txt_title.text = "JUPITER";
		yield return new WaitForSeconds (waitTime);

        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
        Distance.GetComponent<Text> ().enabled = true;
		Distanceinfo.GetComponent<Text> ().enabled = true;
		txt_Distancefromsun.text = "DISTANCE FROM SUN :";
		txt_Distanceinfo.text = "778 MILLION KILOMETERS";
		yield return new WaitForSeconds (waitTime);

        audio.Play();
        Hours.GetComponent<Text> ().enabled = true;
		Hoursinfo.GetComponent<Text> ().enabled = true;
		txt_Hoursperday.text = "HOURS PER DAY :";
		txt_Hoursinfo.text = "10 EARTH HOURS";
		yield return new WaitForSeconds (waitTime);

        audio.Play();
        Days.GetComponent<Text> ().enabled = true;
		Daysinfo.GetComponent<Text> ().enabled = true;
		txt_Daysperyear.text = "DAYS PER YEAR :";
		txt_Daysinfo.text = "12 EARTH YEARS";
		yield return new WaitForSeconds (waitTime);

        audio.Play();
        Diameter.GetComponent<Text> ().enabled = true;
		Diameterinfo.GetComponent<Text> ().enabled = true;
		txt_Diameter.text = "DIAMETER :";
		txt_Diameterinfo.text = "1,42,800 KILOMETERS";
		yield return new WaitForSeconds (waitTime);

        audio.Play();
        Moons.GetComponent<Text> ().enabled = true;
		Moonsinfo.GetComponent<Text> ().enabled = true;
		txt_Numberofmoons.text = "NUMBER OF MOONS :";
		txt_Moonsinfo.text = "63 MOONS";
		yield return new WaitForSeconds (waitTime);

        audio.Play();
        Temperature.GetComponent<Text> ().enabled = true;
		Temperatureinfo.GetComponent<Text> ().enabled = true;
		txt_Temperature.text = "TEMPERATURE :";
		txt_Temperatureinfo.text = "-150 DEGREE CELCIUS";
		yield return new WaitForSeconds (waitTime);

	}

	IEnumerator Saturan(float waitTime){
		
		Title.GetComponent<Text> ().enabled = true;
		txt_title.text = "SATURN";
		yield return new WaitForSeconds (waitTime);

        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
        Distance.GetComponent<Text> ().enabled = true;
		Distanceinfo.GetComponent<Text> ().enabled = true;
		txt_Distancefromsun.text = "DISTANCE FROM SUN :";
		txt_Distanceinfo.text = "1727 MILLION KILOMETERS";
		yield return new WaitForSeconds (waitTime);

        audio.Play();
        Hours.GetComponent<Text> ().enabled = true;
		Hoursinfo.GetComponent<Text> ().enabled = true;
		txt_Hoursperday.text = "HOURS PER DAY :";
		txt_Hoursinfo.text = "10.5 EARTH HOURS";
		yield return new WaitForSeconds (waitTime);

        audio.Play();
        Days.GetComponent<Text> ().enabled = true;
		Daysinfo.GetComponent<Text> ().enabled = true;
		txt_Daysperyear.text = "DAYS PER YEAR :";
		txt_Daysinfo.text = "29 EARTH YEARS";
		yield return new WaitForSeconds (waitTime);

        audio.Play();
        Diameter.GetComponent<Text> ().enabled = true;
		Diameterinfo.GetComponent<Text> ().enabled = true;
		txt_Diameter.text = "DIAMETER :";
		txt_Diameterinfo.text = "1,20,665 KILOMETERS";
		yield return new WaitForSeconds (waitTime);

        audio.Play();
        Moons.GetComponent<Text> ().enabled = true;
		Moonsinfo.GetComponent<Text> ().enabled = true;
		txt_Numberofmoons.text = "NUMBER OF MOONS :";
		txt_Moonsinfo.text = "60 MOONS";
		yield return new WaitForSeconds (waitTime);

        audio.Play();
        Temperature.GetComponent<Text> ().enabled = true;
		Temperatureinfo.GetComponent<Text> ().enabled = true;
		txt_Temperature.text = "TEMPERATURE :";
		txt_Temperatureinfo.text = "-180 DEGREE CELCIUS";
		yield return new WaitForSeconds (waitTime);

	}

	IEnumerator Uranus(float waitTime){
		
		Title.GetComponent<Text> ().enabled = true;
		txt_title.text = "URANUS";
		yield return new WaitForSeconds (waitTime);

        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
        Distance.GetComponent<Text> ().enabled = true;
		Distanceinfo.GetComponent<Text> ().enabled = true;
		txt_Distancefromsun.text = "DISTANCE FROM SUN :";
		txt_Distanceinfo.text = "2781 MILLION KILOMETERS";
		yield return new WaitForSeconds (waitTime);

        audio.Play();
        Hours.GetComponent<Text> ().enabled = true;
		Hoursinfo.GetComponent<Text> ().enabled = true;
		txt_Hoursperday.text = "HOURS PER DAY :";
		txt_Hoursinfo.text = "17 EARTH HOURS";
		yield return new WaitForSeconds (waitTime);

        audio.Play();
        Days.GetComponent<Text> ().enabled = true;
		Daysinfo.GetComponent<Text> ().enabled = true;
		txt_Daysperyear.text = "DAYS PER YEAR :";
		txt_Daysinfo.text = "84 EARTH YEARS";
		yield return new WaitForSeconds (waitTime);

        audio.Play();
        Diameter.GetComponent<Text> ().enabled = true;
		Diameterinfo.GetComponent<Text> ().enabled = true;
		txt_Diameter.text = "DIAMETER :";
		txt_Diameterinfo.text = "51,809 KILOMETERS";
		yield return new WaitForSeconds (waitTime);

        audio.Play();
        Moons.GetComponent<Text> ().enabled = true;
		Moonsinfo.GetComponent<Text> ().enabled = true;
		txt_Numberofmoons.text = "NUMBER OF MOONS :";
		txt_Moonsinfo.text = "27 MOONS";
		yield return new WaitForSeconds (waitTime);

        audio.Play();
        Temperature.GetComponent<Text> ().enabled = true;
		Temperatureinfo.GetComponent<Text> ().enabled = true;
		txt_Temperature.text = "TEMPERATURE :";
		txt_Temperatureinfo.text = "-214 DEGREE CELCIUS";
		yield return new WaitForSeconds (waitTime);

	}

	IEnumerator Neptune(float waitTime){

		Title.GetComponent<Text> ().enabled = true;
		txt_title.text = "NEPTUNE";
		yield return new WaitForSeconds (waitTime);

        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
        Distance.GetComponent<Text> ().enabled = true;
		Distanceinfo.GetComponent<Text> ().enabled = true;
		txt_Distancefromsun.text = "DISTANCE FROM SUN :";
		txt_Distanceinfo.text = "4504 MILLION KILOMETERS";
		yield return new WaitForSeconds (waitTime);

        audio.Play();
        Hours.GetComponent<Text> ().enabled = true;
		Hoursinfo.GetComponent<Text> ().enabled = true;
		txt_Hoursperday.text = "HOURS PER DAY :";
		txt_Hoursinfo.text = "16 EARTH HOURS";
		yield return new WaitForSeconds (waitTime);

        audio.Play();
        Days.GetComponent<Text> ().enabled = true;
		Daysinfo.GetComponent<Text> ().enabled = true;
		txt_Daysperyear.text = "DAYS PER YEAR :";
		txt_Daysinfo.text = "165 EARTH YEARS";
		yield return new WaitForSeconds (waitTime);

        audio.Play();
        Diameter.GetComponent<Text> ().enabled = true;
		Diameterinfo.GetComponent<Text> ().enabled = true;
		txt_Diameter.text = "DIAMETER :";
		txt_Diameterinfo.text = "49,530 KILOMETERS";
		yield return new WaitForSeconds (waitTime);

        audio.Play();
        Moons.GetComponent<Text> ().enabled = true;
		Moonsinfo.GetComponent<Text> ().enabled = true;
		txt_Numberofmoons.text = "NUMBER OF MOONS :";
		txt_Moonsinfo.text = "11 MOONS";
		yield return new WaitForSeconds (waitTime);

        audio.Play();
        Temperature.GetComponent<Text> ().enabled = true;
		Temperatureinfo.GetComponent<Text> ().enabled = true;
		txt_Temperature.text = "TEMPERATURE :";
		txt_Temperatureinfo.text = "-220 DEGREE CELCIUS";
		yield return new WaitForSeconds (waitTime);

	}

	IEnumerator Pluto(float waitTime){
		
		Title.GetComponent<Text> ().enabled = true;
		txt_title.text = "PLUTO";
		yield return new WaitForSeconds (waitTime);

        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
        Distance.GetComponent<Text> ().enabled = true;
		Distanceinfo.GetComponent<Text> ().enabled = true;
		txt_Distancefromsun.text = "DISTANCE FROM SUN :";
		txt_Distanceinfo.text = "5900 MILLION KILOMETERS";
		yield return new WaitForSeconds (waitTime);

        audio.Play();
        Hours.GetComponent<Text> ().enabled = true;
		Hoursinfo.GetComponent<Text> ().enabled = true;
		txt_Hoursperday.text = "HOURS PER DAY :";
		txt_Hoursinfo.text = "6.5  EARTH HOURS";
		yield return new WaitForSeconds (waitTime);

        audio.Play();
        Days.GetComponent<Text> ().enabled = true;
		Daysinfo.GetComponent<Text> ().enabled = true;
		txt_Daysperyear.text = "DAYS PER YEAR :";
		txt_Daysinfo.text = "249 EARTH YEARS";
		yield return new WaitForSeconds (waitTime);

        audio.Play();
        Diameter.GetComponent<Text> ().enabled = true;
		Diameterinfo.GetComponent<Text> ().enabled = true;
		txt_Diameter.text = "DIAMETER :";
		txt_Diameterinfo.text = "2,290 KILOMETERS";
		yield return new WaitForSeconds (waitTime);

        audio.Play();
        Moons.GetComponent<Text> ().enabled = true;
		Moonsinfo.GetComponent<Text> ().enabled = true;
		txt_Numberofmoons.text = "NUMBER OF MOONS :";
		txt_Moonsinfo.text = "3 MOONS";
		yield return new WaitForSeconds (waitTime);

        audio.Play();
        Temperature.GetComponent<Text> ().enabled = true;
		Temperatureinfo.GetComponent<Text> ().enabled = true;
		txt_Temperature.text = "TEMPERATURE :";
		txt_Temperatureinfo.text = "-230 DEGREE CELCIUS";
		yield return new WaitForSeconds (waitTime);

	}
}


