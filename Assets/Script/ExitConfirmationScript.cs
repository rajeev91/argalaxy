﻿using UnityEngine;
using System.Collections;

public class ExitConfirmationScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            MobileNativeDialog dialog = new MobileNativeDialog("CONFIRM EXIT", "Do you want to quit the game?");
            //   Application.Quit(); 
            Time.timeScale = 0;

            dialog.OnComplete += OnDialogClose;
        }
    }
    private void OnDialogClose(MNDialogResult result)
    {
        switch (result)
        {
            case MNDialogResult.YES:
                //Debug.Log("Yes button pressed"); 
                Application.Quit();
                break;
            case MNDialogResult.NO:
                Debug.Log("No button pressed");
                Time.timeScale = 1;
                break;
        }
    }
}
